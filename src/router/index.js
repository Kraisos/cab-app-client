import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Users from "../views/Users.vue";
import Trainings from "../views/Trainings.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/users",
    name: "users",
    component: Users
  },
  {
    path: "/trainings",
    name: "trainings",
    component: Trainings
  }
]

const router = new VueRouter({
  routes
})

export default router
