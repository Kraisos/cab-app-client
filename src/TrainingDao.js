import axios from "axios";

const url = "/api/trainings/";

class TrainingDao {
  static getTrainings() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url);
        const data = res.data;
        const mappedData = data.map(training => ({
          ...training,
          date: new Date(training.date),
          createdAt: new Date(training.createdAt)
        }));
        //console.log(data);
        //console.log(mappedData);
        resolve(mappedData);
      } catch (err) {
        reject(err);
      }
    });
  }

  static createTraining(training) {
    return axios.post(url, training).catch(function(error) {
      alert(error);
    });
  }

  static deleteTraining(id) {
    return axios.delete(`${url}${id}`);
  }
}

export default TrainingDao;
