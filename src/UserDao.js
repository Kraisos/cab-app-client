import axios from "axios";

const url = "/api/users/";

class UserDao {
  static getUsers() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url);
        const data = res.data;
        const mappedData = data.map(user => ({
          ...user,
          createdAt: new Date(user.createdAt)
        }));
        //console.log(data);
        //console.log(mappedData);
        resolve(mappedData);
      } catch (err) {
        reject(err);
      }
    });
  }

  static createUser(user, role) {
    return axios.post(url + "?role=" + role, user).catch(function(error) {
      alert(error);
    });
  }

  static deleteUser(id) {
    return axios.delete(`${url}${id}`);
  }
}

export default UserDao;
